import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    Button,
    Image
  } from 'react-native';
  import { Actions } from "react-native-router-flux";
  import { createStore} from 'redux';
  import { connect, Provider} from 'react-redux';

  const TYPE_USER_ID = 'TYPE_USER_ID';
  const typesUser_id = (text) => ({
    type: TYPE_USER_ID,
    text
  })

  const LoginFormTextInput = connect((state) => ({
    value: state.user_id
  }),(dispatch) => ({ 
    onChangeText: (text) => {
      dispatch(typesUser_id(text));
    }
  }))(TextInput)

  const TYPE_USER_PASSWORD = 'TYPE_USER_PASSWORD';
  const typesUser_password = (text1) => ({
    type: TYPE_USER_PASSWORD,
    text1
  })

  const LoginFormPassTextInput = connect((state) => ({
    value: state.user_password
  }),(dispatch) => ({ 
    onChangeText: (text1) => {
      dispatch(typesUser_password(text1));
    }
  }))(TextInput)

export default class Next extends Component {
  constructor(props) {
    super(props);
    this.store = createStore((state, action) => {
      console.log(JSON.stringify(action))
      return {...state, user_id: action.text}
    }, this.state)
    this.store = createStore((state, action) => {
      console.log(JSON.stringify(action))
      return {...state, user_password: action.text1}
    }, this.state)
    this.state = {
       user_id: '',
       user_password: '',
       pendingLoginRequest: false
    };
  };
  
  render(){
    const Home=()=>{
        Actions.Home()
    }

  return (
    <Provider store={this.store}>
      <View style={styles.container}>
      <TouchableOpacity onPress={Home}></TouchableOpacity>
      <Image style={{width: 170, height: 130}}
          source={require('./Coffee.jpg')}/>
      <Text>user_id</Text>
      <LoginFormTextInput style={styles.inputBox}
          underlineColorAndroid='rgba(0,0,0,0)'
          placeholder='Email'
          placeholderTextColor='black'
        />
      <Text>user_password</Text>
      <LoginFormPassTextInput style={styles.inputBox}
          underlineColorAndroid='rgba(0,0,0,0)'
          placeholder='Password'
          secureTextEntry={true}
          placeholderTextColor='black'
        />
      <TouchableOpacity style={styles.button}>
      <Button title="Login"  />
      <Text style={styles.buttonText}>{this.props.title}</Text>
      </TouchableOpacity>
      </View>
    </Provider>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    inputBox: {
        width:300,
        backgroundColor:'rgba(255,0,0,0.3)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color:'#ffffff',
        marginVertical: 10   
    },
    button:{
      width:300,
      borderRadius: 25,
      marginVertical: 10,
      paddingVertical: 13
    },
    buttonText: {
      fontSize: 16,
      fontWeight:'500',
      color:'#ffffff',
      textAlign: 'center'
    }
  });
     