import React, {Components} from 'react';
import {Router, Scene} from 'react-native-router-flux';
import Home from "./Home";
import Next from "./Next";
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    ScrollView,
    TouchableOpacity,
  } from 'react-native';

const Routes=()=>(
    <Router>
        <Scene >
            <Scene key="Home" component={Home}  initial={true}/>
            <Scene key="Next" component={Next}  />
        </Scene>
    </Router>
)
export default Routes;
