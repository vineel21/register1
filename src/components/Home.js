import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Button,
    TouchableOpacity,
  } from 'react-native';
import { Actions } from "react-native-router-flux";
import { createStore} from 'redux';
import { connect, Provider} from 'react-redux';

  const TYPE_F_NAME = 'TYPE_F_NAME';
  const typesF_name = (text) => ({
    type: TYPE_F_NAME,
    text
  })

  const LoginFormTextInput = connect((state) => ({
    value: state.f_name
  }),(dispatch) => ({ 
    onChangeText: (text) => {
      dispatch(typesF_name(text));
    }
  }))(TextInput)

  const TYPE_M_NAME = 'TYPE_M_NAME';
  const typesM_name = (text1) => ({
    type: TYPE_M_NAME,
    text1
  })

  const LoginFormMiddleTextInput = connect((state) => ({
    value: state.m_name
  }),(dispatch) => ({ 
    onChangeText: (text1) => {
      dispatch(typesM_name(text1));
    }
  }))(TextInput)

  const TYPE_USER_ID = 'TYPE_USER_ID';
  const typesUser_id = (text2) => ({
    type: TYPE_USER_ID,
    text2
  })

  const LoginFormUserTextInput = connect((state) => ({
    value: state.user_id
  }),(dispatch) => ({ 
    onChangeText: (text2) => {
      dispatch(typesUser_id(text2));
    }
  }))(TextInput)

  const TYPE_USER_PASSWORD = 'TYPE_USER_PASSWORD';
  const typesUser_password = (text3) => ({
    type: TYPE_USER_PASSWORD,
    text3
  })

  const LoginFormPassTextInput = connect((state) => ({
    value: state.user_password
  }),(dispatch) => ({ 
    onChangeText: (text3) => {
      dispatch(typesUser_password(text3));
    }
  }))(TextInput)

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.store = createStore((state, action) => {
      console.log(JSON.stringify(action))
      return {...state, f_name: action.text}
    }, this.state)
    this.store = createStore((state, action) => {
      console.log(JSON.stringify(action))
      return {...state, m_name: action.text1}
    }, this.state)
    this.store = createStore((state, action) => {
      console.log(JSON.stringify(action))
      return {...state, user_id: action.text2}
    }, this.state)
    this.store = createStore((state, action) => {
      console.log(JSON.stringify(action))
      return {...state, user_password: action.text3}
    }, this.state)
    this.state = {
      f_name: '',
      m_name: '',
      user_id: '',
      user_password: '',
      pendingRegisterRequest: false
   };
  };
  render(){
    const Next=()=>{
        Actions.Next()
    }

  return (
    <Provider store={this.store}>
    <View style={styles.container}>
        <Text>f_name</Text>
        <LoginFormTextInput style={styles.inputBox}
          underlineColorAndroid='rgba(0,0,0,0)'
          placeholder='FirstName'
          placeholderTextColor='black'
          />
        <Text>m_name</Text>
        <LoginFormMiddleTextInput style={styles.inputBox}
          underlineColorAndroid='rgba(0,0,0,0)'
          placeholder='MiddleName'
          placeholderTextColor='black'
          />
        <Text>user_id</Text>
        <LoginFormUserTextInput style={styles.inputBox}
          underlineColorAndroid='rgba(0,0,0,0)'
          placeholder='Username'
          placeholderTextColor='black'
          />
        <Text>user_password</Text>
        <LoginFormPassTextInput style={styles.inputBox}
          underlineColorAndroid='rgba(0,0,0,0)'
          placeholder='Password'
          secureTextEntry={true}
          placeholderTextColor='black'
          />
        <TouchableOpacity style={styles.button}>
        <Button title="Register"  />
          <Text style={styles.buttonText}>{this.props.title}</Text>
          <TouchableOpacity onPress={Next}><Text>Login</Text></TouchableOpacity>
        </TouchableOpacity>
    </View>
    </Provider>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    inputBox: {
        width:300,
        backgroundColor:'rgba(255,0,0,0.3)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color:'#ffffff',
        marginVertical: 10   
    },
    button:{
      width:300,
      borderRadius: 25,
      marginVertical: 10,
      paddingVertical: 13
    },
    buttonText: {
      fontSize: 16,
      fontWeight:'500',
      color:'#ffffff',
      textAlign: 'center'
    }
  });
     